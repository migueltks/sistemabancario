package com.ks.bancario.accounts.credits;

import com.ks.bancario.accounts.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by migue on 16/11/2016.
 */
public class Credit extends Account
{
    private static final Logger VMobjLog = LogManager.getLogger(Credit.class);

    private static int contador;
    protected float interes;
    protected float limit;

    static
    {
        contador = 0;
    }

    protected Credit()
    {
        contador += 1;
        interes = 5;
        limit = 5000;
        balance = 5000;
        if (VMobjLog.isDebugEnabled())
        {
            VMobjLog.debug("Se han creado " + contador + " cuentas de credito");
        }
    }

    public float getInteres()
    {
        return interes;
    }

    public void setInteres(float interes)
    {
        this.interes = interes;
    }

    public float getLimit()
    {
        return limit;
    }

    public void setLimit(float limit)
    {
        this.limit = limit;
    }

    public boolean subStract(float amount)
    {
        balance -= amount * interes / 100;
        return true;
    }
}
