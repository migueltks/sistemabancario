package com.ks.bancario.databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by migue on 08/12/2016.
 */
public class SQLlite extends Database implements DbConnection
{
    public boolean connect()
    {
        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;


        try
        {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + base);
            statement = connection.createStatement();
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

    }

    public boolean insert(String query)
    {
        return false;
    }

    public boolean upadte(String query)
    {
        return false;
    }

    public boolean isConnected()
    {
        return false;
    }
}
